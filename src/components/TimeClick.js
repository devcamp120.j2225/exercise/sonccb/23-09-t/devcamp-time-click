import { Component } from "react";

class TimeClick extends Component {
     constructor(props) {
          super(props);
          this.state = {
               time: []
          }
     }

     onBtnAddTime = () => {
          let today = new Date();
          let hours = today.getHours();
          let minutes = today.getMinutes();
          let seconds = today.getSeconds();
          let halfDay = hours < 12 ? 'AM' : 'PM';

          let addTime = {};

          addTime = `${hours}:${minutes}:${seconds} ${halfDay}`;
          this.setState({
               time: [...this.state.time, addTime]
          })
     }

     render() {
          return (
               <div>
                    <p>List:</p>
                    <ol>
                         {this.state.time.map((element, index) => {
                              return <li key={index}>{element}</li>
                         })}
                    </ol>
                    <button onClick={this.onBtnAddTime}>Add to List</button>
               </div>
          )
     }
}

export default TimeClick;
